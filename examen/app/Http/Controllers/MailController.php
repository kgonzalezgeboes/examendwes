<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\Contacto;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
    public function enviarMail(Request $request) {
        Mail::to("keigongg99@gmail.com")->send(new Contacto($request->post("titulo"), $request->post("mensaje")));

        return view('mailcorrect');
    }
}
