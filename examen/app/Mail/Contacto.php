<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Contacto extends Mailable
{
    use Queueable, SerializesModels;


    public $titulo, $mensaje;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(String $titulo, String $mensaje)
    {
        $this->titulo = $titulo;
        $this->mensaje = $mensaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('kgonzalezgeboes@iessonferrer.net')
                ->view('mail.contacto');
    }
}
