<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
        integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
</head>

<body class="d-flex flex-column h-100">
    @include('templates.header')

    <!-- Begin page content -->
    <main class="flex-shrink-0 mt-5">
        <div class="container mt-5">
            <h1>Bienvenido!!!!</h1>
            <img src="https://images.pexels.com/photos/1101204/pexels-photo-1101204.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940" width="200" height="200" alt="Hormiga">
        </div>
    </main>

    @include('templates.footer')

    <script src="/docs/5.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous">
    </script>
</body>

</html>
